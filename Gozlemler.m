% Bedri KESK�N bedri.keskin@gmail.com
% Ankara �niversitesi Fen Bilimleri Enstit�s�
% Astronomi ve Uzay Bilimleri Anabilim Dal�
% 
clc % komut ekran�n� temizle
clear % haf�zay� temizle

load('Gozlemler.mat');
load('Sonuc.mat');
T0lfs = cell2mat(Sonuc(1,2));
Plfs = cell2mat(Sonuc(2,2));

MinZamII20140409=2456757.3932;
MinZamI20140421= 2456769.4506;
MinZamII20140510=2456788.4661;
MinZamI20150630= 2457204.4425;
MinZamI20180403= 2458212.5009;

% load('Parametre.mat')
% HangiSutun = 2; %Parametre.mat'daki farkl� s�tunlardaki parametreleri kolayca verebilmek i�in.
% T0 = cell2mat(Parametre(1,HangiSutun));
% P = cell2mat(Parametre(2,HangiSutun));
% T0 = T0lfs;
% P = Plfs;
T0 = 2452502.525;
P = 3.45222;

figure(41)
clf
title(MinZamII20140409);
subplot(2,2,1);
IECiz(II20140409(:,1:2), T0, P, MinZamII20140409)% B band�
subplot(2,2,2);
IECiz(II20140409(:,3:4), T0, P, MinZamII20140409)% V band�
subplot(2,2,3);
IECiz(II20140409(:,5:6), T0, P, MinZamII20140409)% R band�
subplot(2,2,4);
IECiz(II20140409(:,7:8), T0, P, MinZamII20140409)% I band�

figure(42)
clf
title(MinZamI20140421);
subplot(2,2,1);
IECiz(I20140421(:,1:2), T0, P, MinZamI20140421)% B band�
subplot(2,2,2);
IECiz(I20140421(:,3:4), T0, P, MinZamI20140421)% V band�
subplot(2,2,3);
IECiz(I20140421(:,5:6), T0, P, MinZamI20140421)% R band�
subplot(2,2,4);
IECiz(I20140421(:,7:8), T0, P, MinZamI20140421)% I band�

figure(43)
clf
title(MinZamII20140510);
subplot(2,2,1);
IECiz(II20140510(:,1:2), T0, P, MinZamII20140510)% B band�
subplot(2,2,2);
IECiz(II20140510(:,3:4), T0, P, MinZamII20140510)% V band�
subplot(2,2,3);
IECiz(II20140510(:,5:6), T0, P, MinZamII20140510)% R band�
subplot(2,2,4);
IECiz(II20140510(:,7:8), T0, P, MinZamII20140510)% I band�

figure(44)
clf
title(MinZamI20150630);
subplot(2,2,1);
IECiz(I20150630(:,1:2), T0, P, MinZamI20150630)% B band�
subplot(2,2,2);
IECiz(I20150630(:,3:4), T0, P, MinZamI20150630)% V band�
subplot(2,2,3);
IECiz(I20150630(:,5:6), T0, P, MinZamI20150630)% R band�
subplot(2,2,4);
IECiz(I20150630(:,7:8), T0, P, MinZamI20150630)% I band�

figure(45)
clf
title(MinZamI20180403);
subplot(2,2,1);
IECiz(I20180403(:,1:2), T0, P, MinZamI20180403)% B band�
subplot(2,2,2);
IECiz(I20180403(:,3:4), T0, P, MinZamI20180403)% V band�
subplot(2,2,3);
IECiz(I20180403(:,5:6), T0, P, MinZamI20180403)% R band�
subplot(2,2,4);
IECiz(I20180403(:,7:8), T0, P, MinZamI20180403)% I band�