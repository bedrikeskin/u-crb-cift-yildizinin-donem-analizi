function [] = IECiz(GozlemVerisi, T0, P, MinZam)

tamkisim = floor(cell2mat(GozlemVerisi(2,1)));
NoktaAdeti=size(GozlemVerisi,1)-1;
HJD = cell2mat(GozlemVerisi(2:NoktaAdeti,1:1))-tamkisim;
MAG = cell2mat(GozlemVerisi(2:NoktaAdeti,2:2));

plot(HJD,MAG,'b.','MarkerSize',5);
line([MinZam-tamkisim MinZam-tamkisim],[min(MAG) max(MAG)],'Color','r') % minimum zaman�n �izgisi
axis ij
xlabel(['HJD+' num2str(tamkisim)]);
ylabel(GozlemVerisi(1,2),'rotation',0,'FontWeight','bold');

% �stte evre ekseni
oldAxis=gca;
    set(get(oldAxis,'Title'),'Units','Normalized','Position',[0.5 1.05 0]);
    newAxis = axes('position', get(oldAxis, 'position'));
	set(newAxis, 'xaxisLocation', 'top');
    set(oldAxis, 'xaxislocation', 'bottom');
    set(newAxis,'Color',get(oldAxis,'Color'));
    set(oldAxis,'Color','none');
	set(newAxis, 'yTickLabel', []);
	set(oldAxis, 'box', 'off');
	set(newAxis, 'yGrid', 'off');
	set(newAxis, 'xGrid', 'off');
    set(newAxis, 'YAxisLocation', 'right');
    set(oldAxis, 'YAxisLocation', 'left');
    set(newAxis, 'YScale', get(oldAxis, 'YScale'));
    set(newAxis, 'YTick', get(oldAxis, 'YTick'));
    set(newAxis, 'XScale', get(oldAxis, 'XScale'));
    set(newAxis, 'XTick', get(oldAxis, 'XTick'));
    linkaxes([oldAxis newAxis],'xy');
    hlink = linkprop([newAxis, oldAxis], {'Position','YTick','XScale','YScale','YMinorTick'});
    setappdata(newAxis,'Axis_Linkage',hlink);
    set(newAxis,'XTick',get(oldAxis, 'XTick'));
    set(newAxis,'XTickLabel',round(mod((str2double(get(oldAxis,'XTickLabel'))-(T0-tamkisim))/P,1)/0.0001)*0.0001);
    temp=get(gcf,'Children');
    i=temp==newAxis;
    j=temp==oldAxis;
    temp(i)=oldAxis;
    temp(j)=newAxis;
    set(gcf,'Children',temp);
    axes(oldAxis);