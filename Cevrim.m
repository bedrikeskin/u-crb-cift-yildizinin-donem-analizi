% 2 II. min ve 1 I. min birle?tirilerek �evrim elde edildi
clear
clc

load('Parametre.mat')
HangiSutun = 2; %Parametre.mat'daki farkl� s�tunlardaki parametreleri kolayca verebilmek i�in.
T0 = cell2mat(Parametre(1,HangiSutun));
P = cell2mat(Parametre(2,HangiSutun));
% T0 = 2452502.525;
% P = 3.45222;

load('Gozlemler.mat');

MinZamI20180403=2458212.5009;
MinZamI20150630=2457204.4425;
MinZamII20140510=2456788.4661;
MinZamII20140409=2456757.3932;

I20180403 = cell2mat(I20180403(2:end,:));
I20150630 = cell2mat(I20150630(2:end,:));
II20140510 = cell2mat(II20140510(2:end,:));
II20140409 = cell2mat(II20140409(2:end,:));

% II20140409'e I20150630'i yakla�t�r
IIyeIi=(floor((I20150630(1,1)-II20140409(1,1))/P))*P;
I20150630=I20150630-IIyeIi*[ones(size(I20150630,1),1) zeros(size(I20150630,1),1) ones(size(I20150630,1),1) zeros(size(I20150630,1),1) ones(size(I20150630,1),1) zeros(size(I20150630,1),1) ones(size(I20150630,1),1) zeros(size(I20150630,1),1)];
MinZamI20150630=MinZamI20150630-IIyeIi;

% II20140409'e II20140510'i yakla�t�r
IIyeIIi=(floor((II20140510(1,1)-II20140409(1,1))/P)-1)*P;
II20140510=II20140510-IIyeIIi*[ones(size(II20140510,1),1) zeros(size(II20140510,1),1) ones(size(II20140510,1),1) zeros(size(II20140510,1),1) ones(size(II20140510,1),1) zeros(size(II20140510,1),1) ones(size(II20140510,1),1) zeros(size(II20140510,1),1)];
MinZamII20140510=MinZamII20140510-IIyeIIi;

% b?t?n g?zlemleri tek matriste topla
BVRI = vertcat(I20150630, II20140409, II20140510); %matrisleri altalta ekle
BVRI = sortrows(BVRI); %k���kten b�y��e do�ru s�rala
% BVRI = vertcat( {'B' 'MAG_B' 'V' 'MAG_V' 'R' 'MAG_R' 'I' 'MAG_I'}, BVRI); %en ba�a ba�l�klar� ekle

HJDB = BVRI(:,1:1);
EVREB=(HJDB-T0)/P;
MAGB = BVRI(:,2:2);

HJDV = BVRI(:,3:3);
EVREV=(HJDV-T0)/P;
MAGV = BVRI(:,4:4);

HJDR = BVRI(:,5:5);
EVRER=(HJDR-T0)/P;
MAGR = BVRI(:,6:6);

HJDI = BVRI(:,7:7);
EVREI=(HJDI-T0)/P;
MAGI = BVRI(:,8:8);

figure(30)
subplot(4,1,1:1)
plot(EVREB,MAGB,'b.','MarkerSize',5);
ylabel('B','Rotation',0, 'Color','red');
line([(MinZamI20150630-T0)/P (MinZamI20150630-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamII20140409-T0)/P (MinZamII20140409-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamII20140510-T0)/P (MinZamII20140510-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
axis ij

subplot(4,1,2:2)
plot(EVREV,MAGV,'b.','MarkerSize',5);
ylabel('V','Rotation',0, 'Color','red');
line([(MinZamI20150630-T0)/P (MinZamI20150630-T0)/P],[min(MAGV) max(MAGV)], 'Color','red')
line([(MinZamII20140409-T0)/P (MinZamII20140409-T0)/P],[min(MAGV) max(MAGV)], 'Color','red')
line([(MinZamII20140510-T0)/P (MinZamII20140510-T0)/P],[min(MAGV) max(MAGV)], 'Color','red')
axis ij

subplot(4,1,3:3)
plot(EVRER,MAGR,'b.','MarkerSize',5);
ylabel('R','Rotation',0, 'Color','red');
line([(MinZamI20150630-T0)/P (MinZamI20150630-T0)/P],[min(MAGR) max(MAGR)], 'Color','red')
line([(MinZamII20140409-T0)/P (MinZamII20140409-T0)/P],[min(MAGR) max(MAGR)], 'Color','red')
line([(MinZamII20140510-T0)/P (MinZamII20140510-T0)/P],[min(MAGR) max(MAGR)], 'Color','red')
axis ij

subplot(4,1,4:4)
plot(EVREI,MAGI,'b.','MarkerSize',5);
ylabel('I','Rotation',0, 'Color','red');
line([(MinZamI20150630-T0)/P (MinZamI20150630-T0)/P],[min(MAGI) max(MAGI)], 'Color','red')
line([(MinZamII20140409-T0)/P (MinZamII20140409-T0)/P],[min(MAGI) max(MAGI)], 'Color','red')
line([(MinZamII20140510-T0)/P (MinZamII20140510-T0)/P],[min(MAGI) max(MAGI)], 'Color','red')
axis ij
