function [fitresult, gof, object] = TekFit(HJDmin, OeksiC, agirlik)
[xData, yData, weights] = prepareCurveData(HJDmin, OeksiC, agirlik);

ft = fittype( 'pp0+pp1*x+pp2*x.^2+pA*((((1-pexcen^2)./(1+pexcen.*cos((2*atan(sqrt((1+pexcen)/(1-pexcen))*tan((kepler(pexcen,2*pi*(x-pT3)/pP3))/2))))).*sin((2*atan(sqrt((1+pexcen)/(1-pexcen))*tan((kepler(pexcen,2*pi*(x-pT3)/pP3))/2)))+pomega*pi/180)+pexcen.*sin(pomega*pi/180))/sqrt(1-pexcen^2*((cos(pomega*pi/180))^2))))+pAman.*sin(2*pi*(x-pTman)/pPman)', 'independent', 'x', 'dependent', 'y', 'coefficients',{'pp0', 'pp1', 'pp2','pA', 'pP3', 'pT3', 'pexcen', 'pomega', 'pAman', 'pPman', 'pTman'});
opts = fitoptions( ft );
opts.Display = 'Off';
                  % burdaki s�ralama yukardaki parametre s�ralamas�yla ayn�
                  % olacak  pA, pP3, pT3...
% opts.Lower =      [957      -0.0026                  1.10e-10                    0.030              39600             2517597             0.72                196                 0.005               11260               2437085];
% opts.StartPoint = [787.72	-0.00539456303644786    1.61271641860673e-09    0.0312411428420758   39600.3705508722	2517597.69633389    0.726320123612075	197.515683576125    0.0180682551630567               11317.8792568586   2437571.18451171];
opts.StartPoint = [787.72	-0.000648260696562796    1.61271641860673e-09   0.0312411428420758   39612.0668284697	2517578.92484984    0.326320123612075	197                 0.0180682551630567               11317.8792568586   2437571.18451171];
% opts.Upper    =   [960      -0.0022                  1.12e-8                  0.032                39601         	2517598             0.73                200                 0.006                11280               2437095];

opts.Weights = weights;
% opts.Method = 'NonlinearLeastSquares';
% opts.TolX = 1.0e-36;

[fitresult, gof, object] = fit( xData, yData, ft, opts );