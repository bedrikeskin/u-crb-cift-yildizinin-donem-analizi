% Bedri KESK�N bedri.keskin@gmail.com
% Ankara �niversitesi Fen Bilimleri Enstit�s�
% Astronomi ve Uzay Bilimleri Anabilim Dal�
% 
clc; % komut ekran�n� temizle
clear; % workspace temizle
% close all; % b�t�n pencereleri kapat
set(0,'DefaultFigureWindowStyle','docked') % grafikleri edit�r penceresine yuvaland�r
FigureNo=0;

% Sabitler:
Mgunes = 1.99e33; % gr
Rgunes = 6.96e10; % cm
Lgunes = 3.839e33; % erg/s
G = 6.67e-8; % cm^3 gr^-1 s^-2
sigma = 5.67*10^-5; %erg cm^-2 s^-1 K^-4


% parametreleri oku
load('Parametre.mat')
HangiSutun = 2; %Parametre.mat'daki farkl� s�tunlardaki parametreleri kolayca verebilmek i�in.
T0 = cell2mat(Parametre(1,HangiSutun));
P = cell2mat(Parametre(2,HangiSutun));
M1 = cell2mat(Parametre(3,HangiSutun));
M2 = cell2mat(Parametre(4,HangiSutun));
R1 = cell2mat(Parametre(5,HangiSutun));
R2 = cell2mat(Parametre(6,HangiSutun));
T1 = cell2mat(Parametre(7,HangiSutun));
T2 = cell2mat(Parametre(8,HangiSutun));
parallax = cell2mat(Parametre(9,HangiSutun));
p0par = cell2mat(Parametre(10,HangiSutun)); % Parabol fitin ba�lang�� p0 parametresi
p1par = cell2mat(Parametre(11,HangiSutun)); % Parabol fitin ba�lang�� p1 parametresi
p2par = cell2mat(Parametre(12,HangiSutun)); % Parabol fitin ba�lang�� p2 parametresi
Asin = cell2mat(Parametre(13,HangiSutun)); % sin�s fitin ba�lang�� A parametresi
P3sin = cell2mat(Parametre(14,HangiSutun)); % sin�s fitin ba�lang�� P3 parametresi
T3sin = cell2mat(Parametre(15,HangiSutun)); % sin�s fitin ba�lang�� T3 parametresi
excensin = cell2mat(Parametre(16,HangiSutun)); % sin�s fitin ba�lang�� e parametresi
omegasin = cell2mat(Parametre(17,HangiSutun)); % sin�s fitin ba�lang�� omega parametresi
p0tek = cell2mat(Parametre(18,HangiSutun)); % Parabol fitin ba�lang�� p0 parametresi
p1tek = cell2mat(Parametre(19,HangiSutun)); % Parabol fitin ba�lang�� p1 parametresi
p2tek = cell2mat(Parametre(20,HangiSutun)); % Parabol fitin ba�lang�� p2 parametresi
Atek = cell2mat(Parametre(21,HangiSutun)); % sin�s fitin ba�lang�� A parametresi
P3tek = cell2mat(Parametre(22,HangiSutun)); % sin�s fitin ba�lang�� P3 parametresi
T3tek = cell2mat(Parametre(23,HangiSutun)); % sin�s fitin ba�lang�� T3 parametresi
excentek = cell2mat(Parametre(24,HangiSutun)); % sin�s fitin ba�lang�� e parametresi
omegatek = cell2mat(Parametre(25,HangiSutun)); % sin�s fitin ba�lang�� omega parametresi
Amantek = cell2mat(Parametre(26,HangiSutun)); % manyetik fitin ba�lang�� Aman parametresi
Tmantek = cell2mat(Parametre(27,HangiSutun)); % manyetik fitin ba�lang�� Tman parametresi
Pmantek = cell2mat(Parametre(28,HangiSutun)); % manyetik fitin ba�lang�� Pman parametresi
wccd = cell2mat(Parametre(29,HangiSutun)); % a��rl�k ccd
wpe = cell2mat(Parametre(30,HangiSutun)); % a��rl�k pe
wpg = cell2mat(Parametre(31,HangiSutun)); % a��rl�k pg
wvis = cell2mat(Parametre(32,HangiSutun)); % a��rl�k vis

% Minimum zamanlar� oku
load('U_CrB_Mins.mat');
% U_CrB(n,:)  = []; % n. sat�rdaki minimumu sil
U_CrB(1,:)  = []; % ilk ba�l�k sat�r�n� sil
U_CrB = sortrows((U_CrB(:,:)),-1); %b�y�kten k����e do�ru s�rala
AdetTopMin=size(U_CrB,1);

MinimumTuru = cell2mat(U_CrB(:,2));
GozlemTuru = U_CrB(:,3);
Kaynak = U_CrB(:,4);
GozlemHatasi = cell2mat(U_CrB(:,5));
% ---------- O-C de�erlerini hesapla ----------------------------------------------
HJDminO = cell2mat(U_CrB(:,1));
CevrimO = (HJDminO - T0) / P;
CevrimC = round(CevrimO/0.5)*0.5; %Hesaplanan �evrim E' hesab�
HJDminC = CevrimC * P + T0; % Hesaplanan minimum zamanlar
OeksiC = HJDminO - HJDminC;

% ---------- a��rl�klar� set et ------------------------------------------------------------------------------
agirlik=ones(AdetTopMin, 1);
for i=1:AdetTopMin
    if strcmp(U_CrB{i,3}, 'ccd')
        agirlik(i) = wccd;
    elseif strcmp(U_CrB{i,3}, 'pe')
        agirlik(i) = wpe;
    elseif strcmp(U_CrB{i,3}, 'pg')
        agirlik(i) = wpg;
    else
        agirlik(i) = wvis; % g�zlem t�r� ccd,pe,pg de�ilse vis'in a��rl��� neyse onu ver
    end
end

% ---------- U_CrB Tablosunu doldur ------------------------------------------------------------------------------
U_CrB=[U_CrB num2cell(agirlik) num2cell(CevrimO) num2cell(CevrimC) num2cell(HJDminC) num2cell(OeksiC)];

% ---------- ilk O-C grafi�i -----------------------------------------------------------------------
FigureNo=FigureNo+1;
figure(FigureNo)
clf
FigureCiz(AdetTopMin,MinimumTuru,GozlemTuru,Kaynak,GozlemHatasi,HJDminO,OeksiC,'modelyok',P,T0);
%------------------------------------------------------------------------------------
%-------- g�zlemleri ayr� tablolar yap ----------------------------------------------
Adetccd=0;
Adetpe=0;
Adetpg=0;
Adetvis=0;
for i=1:AdetTopMin % g�zlem adetlerini bul
    if strcmp(GozlemTuru{i,1}, 'ccd')
       Adetccd=Adetccd+1;
    end
    if strcmp(GozlemTuru{i,1}, 'pe')
       Adetpe=Adetpe+1;
    end
    if strcmp(GozlemTuru{i,1}, 'pg')
       Adetpg=Adetpg+1;
    end
    if strcmp(GozlemTuru{i,1}, 'vis')
       Adetvis=Adetvis+1;
    end
end

Gozlemccd=cell(Adetccd,10);
Gozlempe=cell(Adetpe,10);
Gozlempg=cell(Adetpg,10);
Gozlemvis=cell(Adetvis,10);

Adetccd=0;
Adetpe=0;
Adetpg=0;
Adetvis=0;

for i=1:AdetTopMin % g�zlem t�rleri i�in ayr� tablolar olu?tur
    if strcmp(U_CrB{i,3}, 'ccd')
        Gozlemccd(Adetccd+1,1:10)= U_CrB(i,1:10);
        Adetccd=Adetccd+1;
    end
     if strcmp(U_CrB{i,3}, 'pe')
        Gozlempe(Adetpe+1,1:10)= U_CrB(i,1:10);
        Adetpe=Adetpe+1;
     end
     if strcmp(U_CrB{i,3}, 'pg')
        Gozlempg(Adetpg+1,1:10)= U_CrB(i,1:10);
        Adetpg=Adetpg+1;
     end
     if strcmp(U_CrB{i,3}, 'vis')
        Gozlemvis(Adetvis+1,1:10)= U_CrB(i,1:10);
        Adetvis=Adetvis+1;
    end
end

%--------vis g�zlemleri tekille�tirme------------------------------------------------------
for i=1:Adetvis % ilk �nce Y�l'lar� hesapla yan�na yaz
    Gozlemvis(i,11)={floor(JulToDate(cell2mat(Gozlemvis(i,1))))};
end

%--------vis'in HJDminO de�erlerini tekille�tir-----------------------
GozlemvistekilHJD = accumarray(cell2mat(Gozlemvis(:,11)),cell2mat(Gozlemvis(:,1)),[],@min);
countHJD = accumarray(cell2mat(Gozlemvis(:,11)),ones(size(cell2mat(Gozlemvis(:,11)))));
HJDminOvistekil = GozlemvistekilHJD(countHJD>=1);

%--------HJDminOvistekil'den CevrimO, CevrimC, HJDminC de�erlerini hesapla-----
CevrimOvistekil = (HJDminOvistekil - T0) / P;
CevrimCvistekil = round(CevrimOvistekil/0.5)*0.5; %Hesaplanan �evrim E' hesab�
HJDminCvistekil = CevrimCvistekil * P + T0; % Hesaplanan minimum zamanlar

%--------vis'in O-C de�erlerini tekille�tir---------------------------
GozlemvistekilOeksiC = accumarray(cell2mat(Gozlemvis(:,11)),cell2mat(Gozlemvis(:,10)),[],@mean);
countOeksiC = accumarray(cell2mat(Gozlemvis(:,11)),ones(size(cell2mat(Gozlemvis(:,11)))));
OeksiCvistekil = GozlemvistekilOeksiC(countOeksiC>=1);

AdetvisT=size(HJDminOvistekil,1);% tekille�mi? vis'lerin say�s�

Kaynakvis=cell(AdetvisT,1);
for i=1:AdetvisT%tekil vis'lerin kaynaklar�n� se�
    for k=1:size(Gozlemvis)
        if HJDminOvistekil(i,1)==cell2mat(Gozlemvis(k,1))
            Kaynakvis(i,1)=Gozlemvis(k,4);
        end
    end
end

Gozlemvistekil=cell(AdetvisT,10);
for i=1:AdetvisT % tekille�mi� Gozlemvis tablosunu tekrar olu�tur
    Gozlemvistekil(i,:)= [{HJDminOvistekil(i,1)} {1} 'vis' Kaynakvis(i,1) {0} {1} {CevrimOvistekil(i,1)} {CevrimCvistekil(i,1)} {HJDminCvistekil(i,1)} {OeksiCvistekil(i,1)}];
end

% B?t?n g�zlemleri tekrar birle�tir % U_CrBT : tekille�tirilmi� U_CrB
U_CrBT = vertcat(Gozlemccd,Gozlempe,Gozlempg,Gozlemvistekil); %matrisleri altalta ekle
U_CrBT = sortrows((U_CrBT(:,:)),-1); %b�y�kten k����e  do�ru s�rala

%------------------------------------------------------------------------
% U_CrBT = U_CrB; % vis tekille�tirmeyi iptal i�in bu sat�r� uncomment et
%------------------------------------------------------------------------

AdetTopMinT=size(U_CrBT,1);
HJDminOT = cell2mat(U_CrBT(:,1));
MinimumTuruT = cell2mat(U_CrBT(:,2));
GozlemTuruT = U_CrBT(:,3);
KaynakT = U_CrBT(:,4);
GozlemHatasiT = cell2mat(U_CrBT(:,5));
agirlikT=cell2mat(U_CrBT(:,6));
CevrimOT=cell2mat(U_CrBT(:,7));
CevrimCT=cell2mat(U_CrBT(:,8));
HJDminCT=cell2mat(U_CrBT(:,9));
OeksiCT=cell2mat(U_CrBT(:,10));
%-------tekille�tirme bitti-------------------------------------------------------
% ---------- tekille�tirilmi� O-C grafi�i -----------------------------------------------------------------------
FigureNo=FigureNo+1;
figure(FigureNo)
clf
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCT,'modelyok',P,T0);
%----------------------------------------------------------------------------------------------------

%---------------------------------------------------------------------------------
% ---------- Lineer Fit (I��k elemanlar�n� d�zeltme)------------------------------
[KatsayiLinFit,RkareLineer,stats] = glmfit(CevrimCT,OeksiCT,'normal');
[KatsayiLinFit,RkareLineer,stats] = glmfit(CevrimCT,zeros(size(OeksiCT)),'normal');% Lineer Fiti iptal etmek (yani ���k elemanlar�n� d�zeltmemek) i�in bu sat�r� uncomment et
dT0 = KatsayiLinFit(1,1);
T0lfshata=stats.se(1,1);
dP = KatsayiLinFit(2,1);
Plfshata=stats.se(2,1);

%%Elle de�er vermek istersen uncomment et
% dT0 = -0.0503554202364636; 
% dP = -1.63818726038115e-05;
% T0lfshata=0;
% Plfshata=0;

% Lineer Fit sonras� yeni ���k elemanlar�
% lfs: lineer fit sonras?
T0lfs = T0 + dT0;
Plfs = P + dP;

lineercizgi=polyval([dP dT0],CevrimCT);
OeksiCTlfs = OeksiCT - lineercizgi;

%Lineer Fit grafi�i
FigureNo=FigureNo+1;
figure(FigureNo)
clf
subplot(10,1,1:6)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCT,lineercizgi,P,T0);
subplot(10,1,8:10)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTlfs,'modelyok',P,T0);

% % --- lineer fit art�klar�yla yeni ���k elemanlar�n�n ayn� sonucu vermesi ---------------------
% % ---------- yeni ���k elemanlar�yla O-C de�erlerini hesapla -----------------------------------------
% HJDminOyie = cell2mat(U_CrBT(:,1));%yie: yeni ���k elemanlar�yla
% CevrimOyie = (HJDminOyie - T0lfs) / Plfs;
% CevrimCyie = round(CevrimOyie/0.5)*0.5; %Hesaplanan �evrim E' hesab�
% HJDminCyie = CevrimCyie * Plfs + T0lfs; % Hesaplanan minimum zamanlar
% OeksiCyie = HJDminOyie - HJDminCyie;
% % 
% % % ---------- yeni ���k elemanlar�yla O-C grafi�i -----------------------------------------------------
% FigureNo=FigureNo+1;
% figure(FigureNo)
% clf
% FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCyie,'modelyok',P,T0);

%----------------------------------------------------------------------------------------------------
% % ---------- Ani D�nem De�i�imi -------------------------------------------------------------------
% % % ---------- Lineer Fit1 ----------- 1:sinir1 -----
sinir1=80;
% sinir1=157;
OeksiCTLinFit1=OeksiCTlfs(1:sinir1,1);
CevrimCLinFit1=CevrimCT(1:sinir1,1);

[KatsayiLinFit1,RkareLineer1,stats1] = glmfit(CevrimCLinFit1,OeksiCTLinFit1,'normal');
T0lf1 = T0lfs + KatsayiLinFit1(1,1);
Plf1 = Plfs + KatsayiLinFit1(2,1);
T0lf1hata=stats1.se(1,1);
Plf1hata=stats1.se(2,1);

lineercizgi1=polyval([KatsayiLinFit1(2,1) KatsayiLinFit1(1,1)],CevrimCLinFit1);
OeksiCadds(1:sinir1,1)= OeksiCTlfs(1:sinir1,1)-lineercizgi1;

% % % ---------- Lineer Fit2 -----------  sinir1:sinir2 ----
sinir2=105;
% sinir2=215;
OeksiCTLinFit2=OeksiCTlfs(sinir1:sinir2,1);
CevrimCLinFit2=CevrimCT(sinir1:sinir2,1);

[KatsayiLinFit2,RkareLineer2,stats2] = glmfit(CevrimCLinFit2,OeksiCTLinFit2,'normal');
T0lf2 = T0lfs + KatsayiLinFit2(1,1);
Plf2 = Plfs + KatsayiLinFit2(2,1);
T0lf2hata=stats2.se(1,1);
Plf2hata=stats2.se(2,1);

lineercizgi2=polyval([KatsayiLinFit2(2,1) KatsayiLinFit2(1,1)],CevrimCLinFit2);
OeksiCadds(sinir1:sinir2,1)= OeksiCTlfs(sinir1:sinir2,1)-lineercizgi2;

% % % ---------- Lineer Fit3 ----------- sinir2:MinimumAdeti -----
OeksiCTLinFit3=OeksiCTlfs(sinir2:AdetTopMinT,1);
CevrimCLinFit3=CevrimCT(sinir2:AdetTopMinT,1);

[KatsayiLinFit3,RkareLineer3,stats3] = glmfit(CevrimCLinFit3,OeksiCTLinFit3,'normal');
T0lf3 = T0lfs + KatsayiLinFit3(1,1);
Plf3 = Plfs + KatsayiLinFit3(2,1);
T0lf3hata=stats3.se(1,1);
Plf3hata=stats3.se(2,1);

lineercizgi3=polyval([KatsayiLinFit3(2,1) KatsayiLinFit3(1,1)],CevrimCLinFit3);
OeksiCadds(sinir2:AdetTopMinT,1)= OeksiCTlfs(sinir2:AdetTopMinT,1)-lineercizgi3;

%--------------------------------------------------
% % ----------- Ani D�nem De�i�im miktar�1
ADD1955=Plf2-Plf3; %g�n
ADD1955hata=sqrt((Plf3hata)^2+(Plf2hata)^2); %g�n
ADD1955sn=ADD1955*24*60*60; %saniye
ADD1955snhata=ADD1955hata*24*60*60; %saniye
%--------------------------------------------------
% % ----------- Ani D�nem De�i�im miktar�2
ADD1970=Plf1-Plf2; %g�n
ADD1970hata=sqrt((Plf2hata)^2+(Plf1hata)^2); %g�n
ADD1970sn=ADD1970*24*60*60; %saniye
ADD1970snhata=ADD1970hata*24*60*60; %saniye
%--------------------------------------------------

%ADD grafi�i
FigureNo=FigureNo+1;
figure(FigureNo)
clf
subplot(10,1,1:6)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTlfs,'modelyok',P,T0);
plot(CevrimCLinFit1,lineercizgi1,'green','LineWidth',1.5)
plot(CevrimCLinFit2,lineercizgi2,'cyan','LineWidth',1.5)
plot(CevrimCLinFit3,lineercizgi3,'red','LineWidth',1.45)
subplot(10,1,8:10)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCadds,'modelyok',P,T0);

RkareADD = sum((OeksiCadds).^2);
%-------------------------------------------------------------------------------------------

%-------------------------------------------------------------------------------------------
% ---------- Parabol Fit -------------------------------------------------------------------
% agirlikPar=agirlikT;
agirlikPar=ones(AdetTopMinT,1);
for i=1:AdetTopMinT
    if strcmp(U_CrBT{i,3}, 'ccd')
        agirlikPar(i) = 10;
    elseif strcmp(U_CrBT{i,3}, 'pe')
        agirlikPar(i) = 1;
    elseif strcmp(U_CrBT{i,3}, 'pg')
        agirlikPar(i) = 1;
    else
        agirlikPar(i) = 1; % g�zlem t�r� ccd,pe,pg de�ilse 1 a��rl��� ver
    end
end

modelfunPar = @(b,x)(b(1)*x.^2+b(2)*x+b(3));
iniKatsayiParFit=[p2par; p1par; p0par];
[KatsayiParFit,~,~,CovBPAr,~,~]=nlinfit(CevrimCT, OeksiCTlfs, modelfunPar, iniKatsayiParFit,'Weights', agirlikPar);
a=KatsayiParFit(1,1);
HataPar=sqrt(diag(CovBPAr));
ahata=HataPar(1,1);

%%Elle de�er vermek istersen uncomment et
% KatsayiParFit=[2.02013532499871e-09;2.42181676978801e-05;0.0302702499925907];
% a=KatsayiParFit(1,1);
% ahata=0;

parabolcizgi=polyval(KatsayiParFit,CevrimCT);
OeksiCTpfs = OeksiCTlfs - parabolcizgi;

%Parabol Fit grafi�i
FigureNo=FigureNo+1;
figure(FigureNo)
clf
subplot(10,1,1:6)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTlfs,parabolcizgi,Plfs,T0lfs);
plot(CevrimCT,parabolcizgi,'blue','LineWidth',1.45)
subplot(10,1,8:10)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTpfs,'modelyok',Plfs,T0lfs);

%D�nem de�i�im miktar�
% isimlerin sonundaki _P parabol fiti ile elde edilen sonu� demektir
deltaPboludeltaE_P = 2*a; % g�n/�evrim, �evrim ba��na
deltaPboludeltaEhata_P = 2*ahata;
deltaP_P = 2*a/Plfs; % g�n/g�n, g�n ba��na
deltaPhata_P = abs(deltaP_P)*sqrt((ahata/a)^2+(Plfshata/Plfs)^2);
deltaPyil_P = deltaP_P * 365.25*24*60*60; % sn/y�l
deltaPyilhata_P = deltaPhata_P * 365.25*24*60*60;
deltaPboluP_P = deltaP_P / Plfs; % deltaP/P  1/g�n
deltaPboluPhata_P = abs(deltaPboluP_P)*sqrt((deltaPhata_P/deltaP_P)^2+(Plfshata/Plfs)^2);

% k�tle aktar�m miktar�
KAMgun_P = deltaPboluP_P*M1*M2/(3*(M1-M2)); % Mg�ne�/g�n
KAMgunhata_P = deltaPboluPhata_P*M1*M2/(3*(M1-M2));
KAMyil_P = KAMgun_P * 365.25; % Mg�ne�/y�l
KAMyilhata_P = KAMgunhata_P * 365.25;

RkareParabol = sum((agirlikPar.*OeksiCTpfs).^2);
%-------------------------------------------------------------------------------------------

% %-------------------------------------------------------------------------------------------
% % ----------Irwin 3. Cisim sin�s Fit ---------------------------------------------------------------------
agirlikSin=agirlikT;
% agirlikSin=ones(AdetTopMinT,1);
% for i=1:AdetTopMinT
%     if strcmp(U_CrBT{i,3}, 'ccd');
%         agirlikSin(i) = 10;
%     elseif strcmp(U_CrBT{i,3}, 'pe');
%         agirlikSin(i) = 1;
%     elseif strcmp(U_CrBT{i,3}, 'pg');
%         agirlikSin(i) = 1;
%     else agirlikSin(i) = 1; % g�zlem t�r� ccd,pe,pg de�ilse 1 a��rl��� ver
%     end
% end

[KatsayiSinFit, gof, objectSinFit]=SinusFit(HJDminOT, OeksiCTpfs, agirlikSin, Asin, P3sin, T3sin, excensin, omegasin);

mse=gof.rmse^2;
J=objectSinFit.Jacobian;
CovBSin = inv(J'*J)*mse;
Sinhata=sqrt(diag(CovBSin));

A_S = KatsayiSinFit.pA; % _S sin�s fitin parametreleri
Ahata_S = Sinhata(1,1);
P3_S = KatsayiSinFit.pP3;
P3hata_S = Sinhata(2,1);
T3_S = KatsayiSinFit.pT3;
T3hata_S = Sinhata(3,1);
excen_S = KatsayiSinFit.pexcen;
excenhata_S = Sinhata(4,1);
omega_S = KatsayiSinFit.pomega;
omegahata_S = Sinhata(5,1);

%%Elle de�er vermek istersen uncomment et
% A_S = 0.03; % _S sin�s fitin parametreleri
% Ahata_S = 0;
% P3_S = 38900;
% P3hata_S = 0;
% T3_S = 2516418;
% T3hata_S = 0;
% excen_S = 0.6;
% excenhata_S = 0;
% omega_S = 200;
% omegahata_S = 0;

sinuscizgi=Irwin(HJDminOT, A_S, P3_S, T3_S, excen_S, omega_S);
OeksiCTsfs = OeksiCTpfs - sinuscizgi;

%sin�s Fit grafi�i
FigureNo=FigureNo+1;
figure(FigureNo)
clf
subplot(10,1,1:6)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTpfs,sinuscizgi,Plfs,T0lfs);
% sinusbaslangiccizgi=Irwin(HJDminOT, Asin, P3sin, T3sin, excensin, omegasin);% ba�lang�� A,P3,T3,e,w de�erleriyle ?izilen sin�s? g?rmek istersen bu 2 sat�r� uncomment et
% plot(HJDminOT,sinusbaslangiccizgi,'black')
plot(HJDminOT,sinuscizgi,'blue','LineWidth',1.45)
subplot(10,1,8:10)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTsfs,'modelyok',Plfs,T0lfs);

RkareSinus = sum((agirlikSin.*OeksiCTsfs).^2);

% % sin�s Fit sonu�lar�
P3yil_S = P3_S / 365.25; %y�l
P3yilhata_S = P3hata_S / 365.25;

bireksiekarecosomegakare_S=1-excen_S^2*cos(omega_S*pi/180)^2;
a12sini_S = (173.15*A_S)/sqrt(bireksiekarecosomegakare_S); %AB
Ayagoreturev_S=1/sqrt(bireksiekarecosomegakare_S);
eyegoreturev_S=-1/2*power(bireksiekarecosomegakare_S,-3/2)*-2*excen_S*cos(omega_S*pi/180)^2;
omegayagoreturev_S=-1/2*power(bireksiekarecosomegakare_S,-3/2)*-excen_S^2*2*cos(omega_S*pi/180)*-sin(omega_S*pi/180);
a12sinihata_S = sqrt((Ayagoreturev_S*Ahata_S)^2+(eyegoreturev_S*excenhata_S)^2+(omegayagoreturev_S*omegahata_S)^2);

KutleFonk_S = a12sini_S^3 / P3yil_S^2; % Mg�ne�
% KutleFonkhata_S = abs(KutleFonk_S)*sqrt((3*a12sinihata_S/a12sini_S)^2+(2*P3yilhata_S/P3yil_S)^2);
KutleFonkhata_S = sqrt((3*a12sini_S^2*a12sinihata_S/P3yil_S^2)^2+(a12sini_S^3*-2*P3yilhata_S/P3yil_S^3)^2);

m3minkokleri_S = roots([1^3 -KutleFonk_S -2*KutleFonk_S*(M1+M2) -KutleFonk_S*(M1+M2)^2]);
m3min_S=m3minkokleri_S(1,1);
m3minhata_S=(KutleFonkhata_S+0.28+0.06)/(KutleFonk_S+M1+M2);% m3hata=(fM3hata+M1hata+M2hata)/(fM3+M1+M2) Hakan hocan�n mailinden
% M1(M?)=	4.74 ? 0.28	Yerli vd. 2003
% M2(M?)=	1.46 ? 0.06	Yerli vd. 2003

a3sini_S=a12sini_S*(M1+M2)/m3min_S;
a3sinihata_S = abs(a3sini_S)*sqrt((a12sinihata_S/a12sini_S)^2+(m3minhata_S/m3min_S)^2);

a123_S=a12sini_S+a3sini_S;
a123hata_S = sqrt(a12sinihata_S^2+a3sinihata_S^2);
%-------------------------------------------------------------------------------------------------------

%----------Applegate Manyetik etki Sinus fiti----------------------------------------------------------------------------
agirlikMan=agirlikT;
% agirlikMan=ones(AdetTopMinT,1);
% for i=1:AdetTopMinT
%     if strcmp(U_CrBT{i,3}, 'ccd');
%         agirlikMan(i) = 10;
%     elseif strcmp(U_CrBT{i,3}, 'pe');
%         agirlikMan(i) = 1;
%     elseif strcmp(U_CrBT{i,3}, 'pg');
%         agirlikMan(i) = 1;
%     else agirlikMan(i) = 1; % g�zlem t�r� ccd,pe,pg de�ilse 1 a��rl��� ver
%     end
% end

modelfunMan = @(b,x)(b(1)*sin(2*pi*(x-b(2))/b(3)));%b1=Aman, b2=Tman, b3=Pman
% iniKatsayiManFit=[p2man; p1man; p0man];
iniKatsayiManFit=[0.005; -810; 3300];
[KatsayiManFit,~,~,CovBMan,~,~]=nlinfit(CevrimCT, OeksiCTsfs, modelfunMan, iniKatsayiManFit,'Weights', agirlikMan);
HataMan=sqrt(diag(CovBMan));
Aman=KatsayiManFit(1,1); % g�n
Amanhata=HataMan(1,1);
Tman=KatsayiManFit(2,1)*Plfs+T0lfs; % HJD, fit �evrime g?re yap?ld??? i�in sonucu P ile ?arp?p T0 toplamak gerek
Tmanhata=HataMan(2,1)*Plfs+T0lfs;
Pman=KatsayiManFit(3,1)*Plfs; % g�n, fit �evrime g?re yap?ld??? i�in sonucu P ile ?arpmak gerek
Pmanhata=HataMan(3,1)*Plfs;

%%Elle de�er vermek istersen uncomment et
% KatsayiManFit=[0.005; 15000; 8000];
% Aman=KatsayiManFit(1,1);
% Tman=KatsayiManFit(2,1);
% Pman=KatsayiManFit(3,1);
% HataMan=0;

manyetikcizgi=(Aman*sin(2*pi*(HJDminCT-Tman)/Pman));
OeksiCTmfs = OeksiCTsfs - manyetikcizgi;

%Manyetik Fit grafi�i
FigureNo=FigureNo+1;
figure(FigureNo)
clf
subplot(10,1,1:6)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTsfs,manyetikcizgi,Plfs,T0lfs);
plot(CevrimCT,manyetikcizgi,'blue','LineWidth',1.45)
subplot(10,1,8:10)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTmfs,'modelyok',Plfs,T0lfs);

RkareMan = sum((agirlikMan.*OeksiCTmfs).^2);
% % Manyetik Fit sonu�lar�
dPman = 86400*2*pi*Plfs*Aman/Pman; %sn, �evrim ba��na D�nem de�i�imi
dPmanhata = abs(dPman)*sqrt((Plfshata/Plfs)^2+(Amanhata/Aman)^2+(Pmanhata/Pman)^2);
M2gr = M2*Mgunes; % gr cinsinden M2 k�tlesi
R2cm = R2*Rgunes; % cm, Raymer 2012
R1cm = R1*Rgunes; % cm, Raymer 2012
a12 = 1.23e12; % cm, Binary Separation, a, Raymer 2012
Bgauss = sqrt((10*G*M2gr^2/R2cm^4)*(a12/R2cm)^2*(dPman/(Pman*86400))); % Gauss, Aktif y�ld�z�n manyetik alan �iddeti
Bgausshata = sqrt(((sqrt((10*G*M2gr^2/R2cm^4)*(a12/R2cm)^2*(1/(Pman*86400)))*0.5*(1/sqrt(dPman)))*dPmanhata)^2+(sqrt((10*G*M2gr^2/R2cm^4)*(a12/R2cm)^2*dPman)*(-0.5)*((Pman*86400)^(-3/2))*Pmanhata)^2);
Btesla = Bgauss/10000;
Bteslahata = Bgausshata/10000;
dJ = -(dPman/(6*pi))*(a12/R2cm)^2*(G*M2gr^2/R2cm); % gr cm^2 s^-1, Manyetik aktif bile�enin i� ve d�� katmanlar� aras�ndaki a��sal momentum transferi
dJhata = abs(-(1/(6*pi))*(a12/R2cm)^2*(G*M2gr^2/R2cm))*dPmanhata;
dE = 3*dJ^2/(M2gr*0.1*R2cm^2); % erg, A��sal momentum transferi i�in gerekli enerji
dEhata = (3/(M2gr*0.1*R2cm^2))*2*dJhata*abs(dJ);
dL = pi*dE/(Pman*86400); % erg/sn, Aktif y�ld�z�n ���n�m g�c�ndeki de�i�im
dLhata = abs(dL)*sqrt((dEhata/dE)^2+(Pmanhata/Pman)^2);
L1 = 4*pi*R1cm^2*sigma*T1^4; % erg/sn, ba� yld�z ���n�m g�c�
L2 = 4*pi*R2cm^2*sigma*T2^4; % erg/sn, yolda� y�ld�z ���n�m g�c�
dm = -2.5*log10((L1+L2)/(L1+L2+dL)); % kadir
dmhata = (1/((L1+L2+dL)*log(10)))*dLhata;

%-------------------------------------------------------------------------------------------------------
%--------- Parabol+sin�s, sin�s, art�klar grafi�i  --------------------------------------------
FigureNo=FigureNo+1;
figure(FigureNo)
clf
subplot(1000,1,1:500)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTlfs,'modelyok',Plfs,T0lfs);
plot(CevrimCT,parabolcizgi,'.','LineWidth',1.3)
plot(CevrimCT,parabolcizgi+sinuscizgi,'--','LineWidth',1.3)
subplot(1000,1,501:800)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTpfs,sinuscizgi,Plfs,T0lfs);
plot(HJDminOT,sinuscizgi,'blue','LineWidth',1.3)
subplot(1000,1,801:1000)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTsfs,'modelyok',Plfs,T0lfs);
%-------------------------------------------------------------------------------------------------------
%--------- Parabol+sin�s+Manyetik, sin�s, Manyetik, art�klar grafi�i  --------------------------------------------
FigureNo=FigureNo+1;
figure(FigureNo)
clf
subplot(1000,1,1:400)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTlfs,'modelyok',Plfs,T0lfs);
plot(CevrimCT,parabolcizgi,'.','LineWidth',1.3)
plot(CevrimCT,parabolcizgi+sinuscizgi+manyetikcizgi,'--','LineWidth',1.3)
subplot(1000,1,401:650)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTpfs,sinuscizgi,Plfs,T0lfs);
plot(HJDminOT,sinuscizgi,'blue','LineWidth',1.3)
subplot(1000,1,651:900)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTsfs,manyetikcizgi,Plfs,T0lfs);
plot(HJDminOT,manyetikcizgi,'blue','LineWidth',1.3)
subplot(1000,1,901:1000)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTmfs,'modelyok',Plfs,T0lfs);
%-------------------------------------------------------------------------------------------------------
%--------- Parabol+sin�s+Manyetik grafi�i --------------------------------------------
FigureNo=FigureNo+1;
figure(FigureNo)
clf
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTlfs,'modelyok',Plfs,T0lfs);
plot(CevrimCT,parabolcizgi,'.','LineWidth',1.3)
% plot(CevrimCT,sinuscizgi,'--','LineWidth',1.3)
plot(CevrimCT,parabolcizgi+sinuscizgi,'--','LineWidth',1.3)
plot(CevrimCT,parabolcizgi+sinuscizgi+manyetikcizgi,'-.','LineWidth',1.3)

%-------------------------------------------------------------------------------------------
% ---------- Tek Fit ---------------------------------------------------------------------
% agirlikTek=agirlikT;
agirlikTek=ones(AdetTopMinT,1);
% for i=1:AdetTopMinT
%     if strcmp(U_CrBT{i,3}, 'ccd');
%         agirlikTek(i) = 10;
%     elseif strcmp(U_CrBT{i,3}, 'pe');
%         agirlikTek(i) = 1;
%     elseif strcmp(U_CrBT{i,3}, 'pg');
%         agirlikTek(i) = 1;
%     else agirlikTek(i) = 1; % g�zlem t�r� ccd,pe,pg de�ilse 1 a��rl��� ver
%     end
% end
% agirlikTek=zeros(AdetTopMinT,1);
% agirlikTek(25-1) = 5;
% % agirlikTek(2-1) = 10;
% agirlikTek(38-1) = 5;
% agirlikTek(68-1) = 5;
% agirlikTek(78-1) = 5;
% agirlikTek(90-1) = 5;
% agirlikTek(141-1) = 5;
% agirlikTek(169-1) = 5;

% U_CrBT=U_CrBT([25 68 90 169],:);

[KatsayiTekFit, gof, objectTekFit]=TekFit(HJDminOT, OeksiCT, agirlikTek);

mse=gof.rmse^2;
J=objectTekFit.Jacobian;
CovBTek = inv(J'*J)*mse;
% CovB = (J'*J)\mse;
HataTek=sqrt(diag(CovBTek));

p0 = KatsayiTekFit.pp0; % _T tek fitin parametreleri
p0hata = HataTek(1,1);
p1 = KatsayiTekFit.pp1; 
p1hata = HataTek(2,1);
p2 = KatsayiTekFit.pp2*P^2; 
p2hata = HataTek(3,1)*P^2;
A_T = KatsayiTekFit.pA; 
Ahata_T = HataTek(4,1);
P3_T = KatsayiTekFit.pP3; 
P3hata_T = HataTek(5,1);
T3_T = KatsayiTekFit.pT3;
T3hata_T = HataTek(6,1);
excen_T = KatsayiTekFit.pexcen; 
excenhata_T = HataTek(7,1);
omega_T = KatsayiTekFit.pomega; 
omegahata_T = HataTek(8,1);
Aman_T = KatsayiTekFit.pAman;
Amanhata_T = HataTek(9,1);
Pman_T = KatsayiTekFit.pPman; 
Pmanhata_T = HataTek(10,1);
Tman_T = KatsayiTekFit.pTman;
Tmanhata_T = HataTek(11,1);

tekfitcizgi=p0+p1*HJDminOT+p2*HJDminOT.^2/P^2+Irwin(HJDminOT, A_T, P3_T, T3_T, excen_T, omega_T)+Aman_T.*sin(2*pi*(HJDminOT-Tman_T)/Pman_T);
OeksiCTtfs = OeksiCT - tekfitcizgi;

% Fit grafi�i
FigureNo=FigureNo+1;
figure(FigureNo)
clf
subplot(10,1,1:6)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCT,tekfitcizgi,P,T0);
subplot(10,1,8:10)
FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTtfs,'modelyok',P,T0);

% RkareTekFit = sum((agirlikTek.*OeksiCTtfs).^2);
RkareTekFit = gof.sse;

%D�nem de�i�im miktar�% tek fit oldu�u i�in P'leri Plfs olarak de�i�tirME sak�n
deltaPboludeltaE_T = 2*p2; % g�n/�evrim, �evrim ba��na
deltaPboludeltaEhata_T = 2*p2hata;
deltaP_T = 2*p2/P; % g�n/g�n
deltaPhata_T = 2*p2hata/P;
deltaPyil_T = deltaP_T * 365.25*24*60*60; % sn/y�l
deltaPyilhata_T = deltaPhata_T * 365.25*24*60*60;
deltaPboluP_T = deltaP_T/P; % deltaP/P  1/g�n
deltaPboluPhata_T = deltaPhata_T/P;

% k�tle aktar�m miktar�
KAMgun_T = deltaPboluP_T*M1*M2/(3*(M1-M2)); % Mg�ne�/g�n
KAMgunhata_T = deltaPboluPhata_T*M1*M2/(3*(M1-M2));
KAMyil_T = KAMgun_T * 365.25; % Mg�ne�/y�l
KAMyilhata_T = KAMgunhata_T * 365.25;

% 3. Cisim sonu�lar�
P3yil_T = P3_T / 365.25; %y�l
P3yilhata_T = P3hata_T / 365.25;

bireksiekarecosomegakare_T=1-excen_T^2*cos(omega_T*pi/180)^2;
a12sini_T = (173.15*A_T)/sqrt(bireksiekarecosomegakare_T); %AB
Ayagoreturev_T=1/sqrt(bireksiekarecosomegakare_T);
eyegoreturev_T=-1/2*power(bireksiekarecosomegakare_T,-3/2)*-2*excen_T*cos(omega_T*pi/180)^2;
omegayagoreturev_T=-1/2*power(bireksiekarecosomegakare_T,-3/2)*-excen_T^2*2*cos(omega_T*pi/180)*-sin(omega_T*pi/180);
a12sinihata_T = sqrt((Ayagoreturev_T*Ahata_T)^2+(eyegoreturev_T*excenhata_T)^2+(omegayagoreturev_T*omegahata_T)^2);

KutleFonk_T = a12sini_T^3 / P3yil_T^2; % Mg�ne�
% KutleFonkhata_T = abs(KutleFonk_T)*sqrt((3*a12sinihata_T/a12sini_T)^2+(2*P3yilhata_T/P3yil_T)^2);
KutleFonkhata_T = sqrt((3*a12sini_T^2*a12sinihata_T/P3yil_T^2)^2+(a12sini_T^3*-2*P3yilhata_T/P3yil_T^3)^2);

m3minkokleri_T = roots([1^3 -KutleFonk_T -2*KutleFonk_T*(M1+M2) -KutleFonk_T*(M1+M2)^2]);
m3min_T=m3minkokleri_T(1,1);
m3minhata_T=(KutleFonkhata_T+0.28+0.06)/(KutleFonk_T+M1+M2);% m3hata=(fM3hata+M1hata+M2hata)/(fM3+M1+M2) Hakan hocan�n mailinden
% M1(M?)=	4.74 ? 0.28	Yerli vd. 2003
% M2(M?)=	1.46 ? 0.06	Yerli vd. 2003

a3sini_T=a12sini_T*(M1+M2)/m3min_T;
a3sinihata_T = abs(a3sini_T)*sqrt((a12sinihata_T/a12sini_T)^2+(m3minhata_T/m3min_T)^2);

a123_T=a12sini_T+a3sini_T;
a123hata_T = sqrt(a12sinihata_T^2+a3sinihata_T^2);

% Manyetik etki sonu�lar�
dPman_T = 86400*2*pi*Plfs*Aman_T/Pman_T; %sn, �evrim ba��na D�nem de�i�imi
dPmanhata_T = abs(dPman_T)*sqrt((Plfshata/Plfs)^2+(Amanhata_T/Aman_T)^2+(Pmanhata_T/Pman_T)^2);

M2gr = M2*Mgunes; % gr cinsinden M2 k�tlesi
R2cm = R2*Rgunes; % cm, Raymer 2012
a12 = 1.23e12; % cm, Binary Separation, a, Raymer 2012
Bgauss_T = sqrt((10*G*M2gr^2/R2cm^4)*(a12/R2cm)^2*(dPman_T/(Pman_T*86400))); % Gauss, Aktif y�ld�z�n manyetik alan �iddeti
Bgausshata_T = sqrt(((sqrt((10*G*M2gr^2/R2cm^4)*(a12/R2cm)^2*(1/(Pman_T*86400)))*0.5*(1/sqrt(dPman_T)))*dPmanhata_T)^2+(sqrt((10*G*M2gr^2/R2cm^4)*(a12/R2cm)^2*dPman_T)*(-0.5)*((Pman_T*86400)^(-3/2))*Pmanhata_T)^2);
Btesla_T = Bgauss_T/10000;
Bteslahata_T = Bgausshata_T/10000;
dJ_T = -(dPman_T/(6*pi))*(a12/R2cm)^2*(G*M2gr^2/R2cm); % gr cm^2 s^-1, Manyetik aktif bile�enin i� ve d�� katmanlar� aras�ndaki a��sal momentum transferi
dJhata_T = abs(-(1/(6*pi))*(a12/R2cm)^2*(G*M2gr^2/R2cm))*dPmanhata_T;
dE_T = 3*dJ_T^2/(M2gr*0.1*R2cm^2); % erg, A��sal momentum transferi i�in gerekli enerji
dEhata_T = (3/(M2gr*0.1*R2cm^2))*2*dJhata_T*abs(dJ_T);
dL_T = pi*dE_T/(Pman_T*86400); % erg/sn, Aktif y�ld�z�n ���n�m g�c�ndeki de�i�im
dLhata_T = abs(dL_T)*sqrt((dEhata_T/dE_T)^2+(Pmanhata_T/Pman_T)^2);
dm_T = -2.5*log10((L1+L2)/(L1+L2+dL_T)); % kadir
dmhata_T = (1/((L1+L2+dL_T)*log(10)))*dLhata_T;

%-------------------------------------------------------------------------------------------------------
%--------- Son art�klar O-C grafi�i --------------------------------------------
% FigureNo=FigureNo+1;
% figure(FigureNo)
% clf
% FigureCiz(AdetTopMinT,MinimumTuruT,GozlemTuruT,KaynakT,GozlemHatasiT,HJDminOT,OeksiCTtfs,'modelyok',P,T0);

% ---------- U_CrB tablosunun gerisini doldur ----------------------------------------
U_CrBT(:,6)=num2cell(agirlikT(:,1));
U_CrBT=[U_CrBT num2cell(OeksiCTlfs) num2cell(OeksiCadds) num2cell(OeksiCTpfs) num2cell(OeksiCTsfs) num2cell(OeksiCTmfs) num2cell(OeksiCTtfs)];
% ---------- U_CrB Tablo ba�l�klar�n� ilk sat�ra ekle --------------------------------
U_CrBT=[{'HJDminO' 'Minimum t�r�' 'g�zlem t�r�' 'Kaynak' 'Hata' 'agirlik' 'CevrimO' 'CevrimC' 'HJDminC' 'OeksiC' 'OeksiClfs' 'OeksiCadds' 'OeksiCpfs' 'OeksiCsfs' 'OeksiCmfs' 'OeksiCtfs'}; U_CrBT];
save('U_CrB_Analiz.mat','U_CrBT'); %Analiz tablosunu diske kaydet

% ---------- sonu�lar� yaz ------------------------------------------------------------------------------
n=1;
Sonuc{n,1}='Parametre';    Sonuc{n,2} = 'de�er';        Sonuc{n,3}='Hata';          Sonuc{n,4}='Birim';     n=n+1;
Sonuc{n,1}='T0';           Sonuc{n,2} = T0;             Sonuc{n,3}='';              Sonuc{n,4}='HJD';       n=n+1;
Sonuc{n,1}='dT0';          Sonuc{n,2} = dT0;            Sonuc{n,3}=T0lfshata;       Sonuc{n,4}='HJD';       n=n+1;
Sonuc{n,1}='T0 son';       Sonuc{n,2} = T0lfs;          Sonuc{n,3}=T0lfshata;       Sonuc{n,4}='HJD';       n=n+1;
Sonuc{n,1}='P';            Sonuc{n,2} = P;              Sonuc{n,3}='';              Sonuc{n,4}='g�n';       n=n+1;
Sonuc{n,1}='dP';           Sonuc{n,2} = dP;             Sonuc{n,3}=Plfshata;        Sonuc{n,4}='g�n';       n=n+1;
Sonuc{n,1}='P son';        Sonuc{n,2} = Plfs;           Sonuc{n,3}=Plfshata;        Sonuc{n,4}='g�n';       n=n+1;
Sonuc{n,1}='ADD1955sn';    Sonuc{n,2} = ADD1955sn;      Sonuc{n,3}=ADD1955snhata;   Sonuc{n,4}='sn';        n=n+1;
Sonuc{n,1}='ADD1970sn';    Sonuc{n,2} = ADD1970sn;      Sonuc{n,3}=ADD1970snhata;   Sonuc{n,4}='sn';        n=n+1;

Sonuc{n,1}='deltaPboludeltaE 2a(parabol fit)';       Sonuc{n,2} = deltaPboludeltaE_P;         Sonuc{n,3}=deltaPboludeltaEhata_P;      Sonuc{n,4}='g�n/�evrim';   n=n+1;
Sonuc{n,1}='deltaPboludeltaE 2a(tek fit)';       Sonuc{n,2} = deltaPboludeltaE_T;         Sonuc{n,3}=deltaPboludeltaEhata_T;      Sonuc{n,4}='g�n/�evrim';   n=n+1;

Sonuc{n,1}='deltaP (parabol fit)';       Sonuc{n,2} = deltaP_P;         Sonuc{n,3}=deltaPhata_P;      Sonuc{n,4}='g�n/g�n';   n=n+1;
Sonuc{n,1}='deltaP (tek fit)';       Sonuc{n,2} = deltaP_T;         Sonuc{n,3}=deltaPhata_T;      Sonuc{n,4}='g�n/g�n';   n=n+1;

Sonuc{n,1}='deltaPyil (parabol fit)' ;   Sonuc{n,2} = deltaPyil_P;      Sonuc{n,3}=deltaPyilhata_P;   Sonuc{n,4}='sn/y�l';    n=n+1;
Sonuc{n,1}='deltaPyil (tek fit)' ;   Sonuc{n,2} = deltaPyil_T;      Sonuc{n,3}=deltaPyilhata_T;   Sonuc{n,4}='sn/y�l';    n=n+1;
Sonuc{n,1}='deltaPboluP (parabol fit)';  Sonuc{n,2} = deltaPboluP_P;    Sonuc{n,3}=deltaPboluPhata_P; Sonuc{n,4}='1/g�n';     n=n+1;
Sonuc{n,1}='deltaPboluP (tek fit)';  Sonuc{n,2} = deltaPboluP_T;    Sonuc{n,3}=deltaPboluPhata_T; Sonuc{n,4}='1/g�n';     n=n+1;
Sonuc{n,1}='KAMgun (parabol fit)';       Sonuc{n,2} = KAMgun_P;         Sonuc{n,3}=KAMgunhata_P;      Sonuc{n,4}='Mg/g�n';    n=n+1;
Sonuc{n,1}='KAMgun (tek fit)';       Sonuc{n,2} = KAMgun_T;         Sonuc{n,3}=KAMgunhata_T;      Sonuc{n,4}='Mg/g�n';    n=n+1;
Sonuc{n,1}='KAMyil (parabol fit)';       Sonuc{n,2} = KAMyil_P;         Sonuc{n,3}=KAMyilhata_P;      Sonuc{n,4}='Mg/y�l';    n=n+1;
Sonuc{n,1}='KAMyil (tek fit)';       Sonuc{n,2} = KAMyil_T;         Sonuc{n,3}=KAMyilhata_T;      Sonuc{n,4}='Mg/y�l';    n=n+1;

Sonuc{n,1}='A (sinus fit)' ;        Sonuc{n,2} = A_S;           Sonuc{n,3}=Ahata_S;        Sonuc{n,4}='g�n';       n=n+1;
Sonuc{n,1}='A (tek fit)' ;        Sonuc{n,2} = A_T;           Sonuc{n,3}=Ahata_T;        Sonuc{n,4}='g�n';       n=n+1;
Sonuc{n,1}='P3 (sinus fit)';        Sonuc{n,2} = P3_S;          Sonuc{n,3}=P3hata_S;       Sonuc{n,4}='g�n';       n=n+1;
Sonuc{n,1}='P3 (tek fit)';        Sonuc{n,2} = P3_T;          Sonuc{n,3}=P3hata_T;       Sonuc{n,4}='g�n';       n=n+1;
Sonuc{n,1}='T3 (sinus fit)';        Sonuc{n,2} = T3_S;          Sonuc{n,3}=T3hata_S;       Sonuc{n,4}='HJD';       n=n+1;
Sonuc{n,1}='T3 (tek fit)';        Sonuc{n,2} = T3_T;          Sonuc{n,3}=T3hata_T;       Sonuc{n,4}='HJD';       n=n+1;
Sonuc{n,1}='excen (sinus fit)';     Sonuc{n,2} = excen_S;       Sonuc{n,3}=excenhata_S;    Sonuc{n,4}='';          n=n+1;
Sonuc{n,1}='excen (tek fit)';     Sonuc{n,2} = excen_T;       Sonuc{n,3}=excenhata_T;    Sonuc{n,4}='';          n=n+1;
Sonuc{n,1}='omega (sinus fit)';     Sonuc{n,2} = omega_S;       Sonuc{n,3}=omegahata_S;    Sonuc{n,4}='derece';    n=n+1;
Sonuc{n,1}='omega (tek fit)';     Sonuc{n,2} = omega_T;       Sonuc{n,3}=omegahata_T;    Sonuc{n,4}='derece';    n=n+1;
Sonuc{n,1}='KutleFonk (sinus fit)';    Sonuc{n,2} = KutleFonk_S;      Sonuc{n,3}=KutleFonkhata_S;   Sonuc{n,4}='Mg';        n=n+1;
Sonuc{n,1}='KutleFonk (tek fit)';    Sonuc{n,2} = KutleFonk_T;      Sonuc{n,3}=KutleFonkhata_T;   Sonuc{n,4}='Mg';        n=n+1;
Sonuc{n,1}='m3min (sinus fit)';        Sonuc{n,2} = m3min_S;          Sonuc{n,3}=m3minhata_S;       Sonuc{n,4}='Mg';        n=n+1;
Sonuc{n,1}='m3min (tek fit)';        Sonuc{n,2} = m3min_T;          Sonuc{n,3}=m3minhata_T;       Sonuc{n,4}='Mg';        n=n+1;
Sonuc{n,1}='P3yil (sinus fit)';        Sonuc{n,2} = P3yil_S;          Sonuc{n,3}=P3yilhata_S;       Sonuc{n,4}='y�l';       n=n+1;
Sonuc{n,1}='P3yil (tek fit)';        Sonuc{n,2} = P3yil_T;          Sonuc{n,3}=P3yilhata_T;       Sonuc{n,4}='y�l';       n=n+1;
Sonuc{n,1}='a12sini (sinus fit)';      Sonuc{n,2} = a12sini_S;        Sonuc{n,3}=a12sinihata_S;     Sonuc{n,4}='AB';        n=n+1;
Sonuc{n,1}='a12sini (tek fit)';      Sonuc{n,2} = a12sini_T;        Sonuc{n,3}=a12sinihata_T;     Sonuc{n,4}='AB';        n=n+1;
Sonuc{n,1}='a3sini (sinus fit)';       Sonuc{n,2} = a3sini_S;         Sonuc{n,3}=a3sinihata_S;      Sonuc{n,4}='AB';        n=n+1;
Sonuc{n,1}='a3sini (tek fit)';       Sonuc{n,2} = a3sini_T;         Sonuc{n,3}=a3sinihata_T;      Sonuc{n,4}='AB';        n=n+1;
Sonuc{n,1}='a123 (sinus fit)';         Sonuc{n,2} = a123_S;           Sonuc{n,3}=a123hata_S;        Sonuc{n,4}='AB';        n=n+1;
Sonuc{n,1}='a123 (tek fit)';         Sonuc{n,2} = a123_T;           Sonuc{n,3}=a123hata_T;        Sonuc{n,4}='AB';        n=n+1;

Sonuc{n,1}='A (man fit)';         Sonuc{n,2} = Aman;           Sonuc{n,3}=Amanhata;        Sonuc{n,4}='g�n';        n=n+1;
Sonuc{n,1}='A (tek fit)';         Sonuc{n,2} = Aman_T;           Sonuc{n,3}=Amanhata_T;        Sonuc{n,4}='g�n';        n=n+1;
Sonuc{n,1}='P (man fit)';         Sonuc{n,2} = Pman;           Sonuc{n,3}=Pmanhata;        Sonuc{n,4}='g�n';        n=n+1;
Sonuc{n,1}='P (tek fit)';         Sonuc{n,2} = Pman_T;           Sonuc{n,3}=Pmanhata_T;        Sonuc{n,4}='g�n';        n=n+1;
Sonuc{n,1}='T (man fit)';         Sonuc{n,2} = Tman;           Sonuc{n,3}=Tmanhata;        Sonuc{n,4}='HJD';        n=n+1;
Sonuc{n,1}='T (tek fit)';         Sonuc{n,2} = Tman_T;           Sonuc{n,3}=Tmanhata_T;        Sonuc{n,4}='HJD';        n=n+1;
Sonuc{n,1}='dP (man fit)';        Sonuc{n,2} = dPman;           Sonuc{n,3}=dPmanhata;        Sonuc{n,4}='sn';        n=n+1;
Sonuc{n,1}='dP (tek fit)';        Sonuc{n,2} = dPman_T;           Sonuc{n,3}=dPmanhata_T;        Sonuc{n,4}='sn';        n=n+1;
Sonuc{n,1}='B (man fit)';         Sonuc{n,2} = Bgauss;           Sonuc{n,3}=Bgausshata;        Sonuc{n,4}='gauss';        n=n+1;
Sonuc{n,1}='B (tek fit)';         Sonuc{n,2} = Bgauss_T;           Sonuc{n,3}=Bgausshata_T;        Sonuc{n,4}='gauss';        n=n+1;
Sonuc{n,1}='B (man fit)';         Sonuc{n,2} = Btesla;           Sonuc{n,3}=Bteslahata;        Sonuc{n,4}='tesla';        n=n+1;
Sonuc{n,1}='B (tek fit)';         Sonuc{n,2} = Btesla_T;           Sonuc{n,3}=Bteslahata_T;        Sonuc{n,4}='tesla';        n=n+1;
Sonuc{n,1}='dJ (man fit)';         Sonuc{n,2} = dJ;           Sonuc{n,3}=dJhata;        Sonuc{n,4}='gr cm^2 s^-1';        n=n+1;
Sonuc{n,1}='dJ (tek fit)';         Sonuc{n,2} = dJ_T;           Sonuc{n,3}=dJhata_T;        Sonuc{n,4}='gr cm^2 s^-1';        n=n+1;
Sonuc{n,1}='dE (man fit)';         Sonuc{n,2} = dE;           Sonuc{n,3}=dEhata;        Sonuc{n,4}='erg';        n=n+1;
Sonuc{n,1}='dE (tek fit)';         Sonuc{n,2} = dE_T;           Sonuc{n,3}=dEhata_T;        Sonuc{n,4}='erg';        n=n+1;
Sonuc{n,1}='dL (man fit)';         Sonuc{n,2} = dL;           Sonuc{n,3}=dLhata;        Sonuc{n,4}='erg/sn';        n=n+1;
Sonuc{n,1}='dL (tek fit)';         Sonuc{n,2} = dL_T;           Sonuc{n,3}=dLhata_T;        Sonuc{n,4}='erg/sn';        n=n+1;
Sonuc{n,1}='dm (man fit)';         Sonuc{n,2} = dm;           Sonuc{n,3}=dmhata;        Sonuc{n,4}='kadir';        n=n+1;
Sonuc{n,1}='dm (tek fit)';         Sonuc{n,2} = dm_T;           Sonuc{n,3}=dmhata_T;        Sonuc{n,4}='kadir';        n=n+1;

Sonuc{n,1}='RkareLineer';  Sonuc{n,2} = RkareLineer;    Sonuc{n,3}='';              Sonuc{n,4}='';          n=n+1;
Sonuc{n,1}='RkareADD';     Sonuc{n,2} = RkareADD;       Sonuc{n,3}='';              Sonuc{n,4}='';          n=n+1;
Sonuc{n,1}='RkareParabol'; Sonuc{n,2} = RkareParabol;   Sonuc{n,3}='';              Sonuc{n,4}='';          n=n+1;
Sonuc{n,1}='RkareSinus';  Sonuc{n,2} = RkareSinus;    Sonuc{n,3}='';              Sonuc{n,4}='';            n=n+1;
Sonuc{n,1}='RkareMan';  Sonuc{n,2} = RkareMan;    Sonuc{n,3}='';              Sonuc{n,4}='';            n=n+1;
Sonuc{n,1}='RkareTekFit';  Sonuc{n,2} = RkareTekFit;    Sonuc{n,3}='';              Sonuc{n,4}='';          
save('Sonuc.mat','Sonuc'); %sonu�lar� diske kaydet

% clear sinir1 sinir2 n lineercizgi lineercizgi1 lineercizgi2 lineercizgi3 parabolcizgi sinuscizgi OeksiCTLinFit1 OeksiCTLinFit2 OeksiCTLinFit3 OeksiCadds KatsayiLinFit1 KatsayiLinFit2 KatsayiLinFit3 ADDcizgi CevrimCLinFit1 CevrimCLinFit2 CevrimCLinFit3 ADD1955 ADD1970 sini CevrimAraligi HJDAraligi TarihAraligi m3kokleri m3minkokleri m3 OeksiCT OeksiCsfs OeksiCT HJDminClfs OeksiC HJDminC CevrimC HangiSutun HJDminO MinimumTuru GozlemTuru agirlik yp1 yp2 yp3 yp33 i text1 FigureNo agirlikLin agirlikPar agirlikSin CevrimO;