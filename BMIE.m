% B�t�n Minimumlar?n I??k E?risi
clear;
clc;

load('Parametre.mat')
HangiSutun = 2; %Parametre.mat'daki farkl� s�tunlardaki parametreleri kolayca verebilmek i�in.
T0 = cell2mat(Parametre(1,HangiSutun));
P = cell2mat(Parametre(2,HangiSutun));
% T0 = 2452502.525;
% P = 3.45222;

load('Gozlemler.mat');

MinZamII20140409=2456757.3932;
MinZamI20140421= 2456769.4506;
MinZamII20140510=2456788.4661;
MinZamI20150630= 2457204.4425;
MinZamI20180403= 2458212.5009;

I20140421 = I20140421(2:end,:);
I20150630 = I20150630(2:end,:);
I20180403 = I20180403(2:end,:);
II20140409 = II20140409(2:end,:);
II20140510 = II20140510(2:end,:);

% b�t�n g�zlemleri tek matriste topla
BVRI = vertcat(I20140421,I20150630,I20180403,II20140409,II20140510); %matrisleri altalta ekle
BVRI = sortrows(BVRI); %k���kten b�y��e do�ru s�rala
% BVRI = vertcat( {'B' 'MAG_B' 'V' 'MAG_V' 'R' 'MAG_R' 'I' 'MAG_I'}, BVRI); %en ba�a ba�l�klar� ekle

% tamkisim = floor(cell2mat(BVRI(1,1))); %HJD'nin noktadan sonra k�sm�n� at

HJDB = cell2mat(BVRI(:,1:1));
EVREB=(HJDB-T0)/P;
MAGB = cell2mat(BVRI(:,2:2));

HJDV = cell2mat(BVRI(:,3:3));
EVREV=(HJDV-T0)/P;
MAGV = cell2mat(BVRI(:,4:4));

HJDR = cell2mat(BVRI(:,5:5));
EVRER=(HJDR-T0)/P;
MAGR = cell2mat(BVRI(:,6:6));

HJDI = cell2mat(BVRI(:,7:7));
EVREI=(HJDI-T0)/P;
MAGI = cell2mat(BVRI(:,8:8));

figure(20)
subplot(4,1,1:1)
plot(EVREB,MAGB,'b.','MarkerSize',5);
ylabel('B','Rotation',0, 'Color','red');
line([(MinZamII20140409-T0)/P (MinZamII20140409-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamI20140421-T0)/P (MinZamI20140421-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamII20140510-T0)/P (MinZamII20140510-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamI20150630-T0)/P (MinZamI20150630-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamI20180403-T0)/P (MinZamI20180403-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
axis ij

subplot(4,1,2:2)
plot(EVREV,MAGV,'b.','MarkerSize',5);
ylabel('V','Rotation',0, 'Color','red');
line([(MinZamII20140409-T0)/P (MinZamII20140409-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamI20140421-T0)/P (MinZamI20140421-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamII20140510-T0)/P (MinZamII20140510-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamI20150630-T0)/P (MinZamI20150630-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamI20180403-T0)/P (MinZamI20180403-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
axis ij

subplot(4,1,3:3)
plot(EVRER,MAGR,'b.','MarkerSize',5);
ylabel('R','Rotation',0, 'Color','red');
line([(MinZamII20140409-T0)/P (MinZamII20140409-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamI20140421-T0)/P (MinZamI20140421-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamII20140510-T0)/P (MinZamII20140510-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamI20150630-T0)/P (MinZamI20150630-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamI20180403-T0)/P (MinZamI20180403-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
axis ij

subplot(4,1,4:4)
plot(EVREI,MAGI,'b.','MarkerSize',5);
ylabel('I','Rotation',0, 'Color','red');
line([(MinZamII20140409-T0)/P (MinZamII20140409-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamI20140421-T0)/P (MinZamI20140421-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamII20140510-T0)/P (MinZamII20140510-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamI20150630-T0)/P (MinZamI20150630-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
line([(MinZamI20180403-T0)/P (MinZamI20180403-T0)/P],[min(MAGB) max(MAGB)], 'Color','red')
axis ij
