function [Result] = JulToDate(juldat) %function to compute Date (year) from Julian data

    L=juldat+68569;
	N=4*L/146097;
	L=L-(146097*N+3)/4;
	I=4000*(L+1)/1461001;
	L=L-1461*I/4+31;
	J=80*L/2447;
	d=L-2447*J/80; %days
	L=J/11;
	m=J+2-12*L; %months
	y=100*(N-49)+I+L; %years
    y=y+1/12;

Result=y;  %result
