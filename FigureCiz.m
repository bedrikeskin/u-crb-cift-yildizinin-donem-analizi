% grafikte sadece minimum noktalar� �izer, fit e�rilerini �izmez.
function [] = FigureCiz(pMinimumAdeti, pMinimumTuru,pGozlemTuru,pKaynak,pGozlemHatasi, HJDminO,pOeksiC,pmodel, pP, pT0)
pCevrimO=(HJDminO-pT0)/pP;
% pGozlemHatasi = pGozlemHatasi*5; % error barlar�n daha belirgin olmas� i�in
for i = pMinimumAdeti:-1:1 %ilk iconu �izmedi�i i�in ccdden de�ilde visden ba�lad�m ki �izilmeyen ccd de�il de vis olsun
    if pMinimumTuru(i,1) == 1
        if strcmp(pGozlemTuru(i,1), 'ccd')
            if strcmp(pKaynak(i,1), 'bu calisma')
                ccdIx = plot(pCevrimO(i,1),pOeksiC(i,1),'diamond','MarkerEdgeColor','g','MarkerFaceColor','r','MarkerSize',10);
            else 
                ccdI = plot(pCevrimO(i,1),pOeksiC(i,1),'square','MarkerEdgeColor','g','MarkerFaceColor','r','MarkerSize',8);
            end
            if  pGozlemHatasi(i,1)>0;   errorbar(pCevrimO(i,1),pOeksiC(i,1),pGozlemHatasi(i,1),'MarkerSize',50,'LineWidth',1.5,'Color','k')
            end
        end
        if strcmp(pGozlemTuru(i,1), 'pe')
            peI = plot(pCevrimO(i,1),pOeksiC(i,1),'o','MarkerEdgeColor','y','MarkerFaceColor','m','MarkerSize',8);
            if  pGozlemHatasi(i,1)>0;   errorbar(pCevrimO(i,1),pOeksiC(i,1),pGozlemHatasi(i,1))
            end
        end
        if strcmp(pGozlemTuru(i,1), 'pg')
            pgI = plot(pCevrimO(i,1),pOeksiC(i,1),'o','MarkerEdgeColor','c','MarkerFaceColor','g','MarkerSize',5);
            if  pGozlemHatasi(i,1)>0;   errorbar(pCevrimO(i,1),pOeksiC(i,1),pGozlemHatasi(i,1))
            end
        end
        if strcmp(pGozlemTuru(i,1), 'vis')
            visI = plot(pCevrimO(i,1),pOeksiC(i,1),'k.','MarkerSize',5);
            if  pGozlemHatasi(i,1)>0;   errorbar(pCevrimO(i,1),pOeksiC(i,1),pGozlemHatasi(i,1))
            end
        end
    end
    if pMinimumTuru(i,1) == 2
        if strcmp(pGozlemTuru(i,1), 'ccd')
            if strcmp(pKaynak(i,1), 'bu calisma')
                ccdIIx = plot(pCevrimO(i,1),pOeksiC(i,1),'diamond','MarkerEdgeColor','k','MarkerFaceColor','c','MarkerSize',10);
            else 
                ccdII =  plot(pCevrimO(i,1),pOeksiC(i,1),'square','MarkerEdgeColor','k','MarkerFaceColor','r','MarkerSize',8);
            end
            if  pGozlemHatasi(i,1)>0;   errorbar(pCevrimO(i,1),pOeksiC(i,1),pGozlemHatasi(i,1),'MarkerSize',50,'LineWidth',1.5,'Color','k')
            end
        end
        if strcmp(pGozlemTuru(i,1), 'pe')
            peII = plot(pCevrimO(i,1),pOeksiC(i,1),'o','MarkerEdgeColor','k','MarkerFaceColor','c','MarkerSize',8);
            if  pGozlemHatasi(i,1)>0;   errorbar(pCevrimO(i,1),pOeksiC(i,1),pGozlemHatasi(i,1))
            end
        end
        if strcmp(pGozlemTuru(i,1), 'pg')
            pgII = plot(pCevrimO(i,1),pOeksiC(i,1),'o','MarkerEdgeColor','r','MarkerFaceColor','y','MarkerSize',5);
            if  pGozlemHatasi(i,1)>0;   errorbar(pCevrimO(i,1),pOeksiC(i,1),pGozlemHatasi(i,1))
            end
        end
        if strcmp(pGozlemTuru(i,1), 'vis')
            visII = plot(pCevrimO(i,1),pOeksiC(i,1),'r.','MarkerSize',5);
            if  pGozlemHatasi(i,1)>0;   errorbar(pCevrimO(i,1),pOeksiC(i,1),pGozlemHatasi(i,1))
            end
        end
    end
    hold all
end

if strcmp(pmodel,'modelyok')
    legend([ccdIIx ccdIx ccdI peII peI pgI visI],'ccd II (bu �alisma)', 'ccd I (bu �alisma)', 'ccd I', 'pe II', 'pe I','pg I', 'vis I', 'Location','EastOutside');
else
model=plot(pCevrimO,pmodel,'blue','LineWidth',1.45);
legend([ccdIIx ccdIx ccdI peII peI pgI visI model],'ccd II (bu �alisma)', 'ccd I (bu �alisma)', 'ccd I', 'pe II', 'pe I','pg I', 'vis I', 'model', 'Location','EastOutside');
end

xlabel('�evrim Say�s�');
ylabel('O-C (g�n)');
% title('U CrB');
% xlim([-10700 5600]);
% xticks([-10000 -8000 -6000 -4000 -2000 0 2000 4000 6000]);
%a�a��daki k�sm�n yaz�lmas�nda
%http://www.mathworks.com/matlabcentral/fileexchange/12131-linktopaxisdata 
%adresinden yararlan�lm��t�r. En son ziyaret: 30.04.2015

%----�ste zaman ekseni ekleme-----
  oldAxis=gca;
    % Shift up the title a bit
    set(get(oldAxis,'Title'),'Units','Normalized','Position',[0.5 1.05 0]);
    % Create new axis
    newAxis = axes('position', get(oldAxis, 'position'));
    % put new Xaxis on top
	set(newAxis, 'xaxisLocation', 'top');
    set(oldAxis, 'xaxislocation', 'bottom');
    % Make the new one the colour of the old, and the old transparent
    set(newAxis,'Color',get(oldAxis,'Color'));
    set(oldAxis,'Color','none');
    % Give the new axis a name if necessary
%     if nargin==3;xlabel(AxisName);end;
    
    % Get rid of y labels on the new axis
	set(newAxis, 'yTickLabel', []);
	% remove box for old axis
	set(oldAxis, 'box', 'off');
	% remove grids
	set(newAxis, 'yGrid', 'off');
	set(newAxis, 'xGrid', 'off');
    % simulate the presence of a box by making the new axis' y axis appear
    % on the right whilst the old one sits on the left
    set(newAxis, 'YAxisLocation', 'right');
    set(oldAxis, 'YAxisLocation', 'left');
    
    set(newAxis, 'YScale', get(oldAxis, 'YScale'));
    set(newAxis, 'YTick', get(oldAxis, 'YTick'));
    set(newAxis, 'XScale', get(oldAxis, 'XScale'));
    set(newAxis, 'XTick', get(oldAxis, 'XTick'));

    linkaxes([oldAxis newAxis],'xy');
    % Create a link between properties
    hlink = linkprop([newAxis, oldAxis], {'Position','YTick','XScale','YScale','YMinorTick'});
    % And store it on the new axis (to make sure it gets updated, but is
    % also destroyed when the figure is closed / axis is deleted)
    setappdata(newAxis,'Axis_Linkage',hlink);
    
    % Label the new axis bits
    set(newAxis,'XTick',get(oldAxis, 'XTick'));
%     set(newAxis,'XTickLabel',round(JulToDate(str2num(get(oldAxis,'XTickLabel'))*pP+pT0)));
    set(newAxis,'XTickLabel',round(JulToDate(str2double(get(oldAxis,'XTickLabel'))*pP+pT0)));
    
    % and finally, swap the places of the two axes so that clicking gives the correct
    % behaviour
    temp=get(gcf,'Children');
    i=temp==newAxis;
    j=temp==oldAxis;
    temp(i)=oldAxis;
    temp(j)=newAxis;
    
    set(gcf,'Children',temp);
    
    axes(oldAxis);
%     pause(2)

%     hold all
