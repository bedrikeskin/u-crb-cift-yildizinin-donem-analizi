function [Result] = kepler(e,M)   %function to compute excentric anomaly from eccentricity and mean anomaly, iterative procedure
%     E=M;
    E=M+e*sin(M);
    E1=E+(M+e*sin(E)-E)./(1-e*cos(E));
    E2=E;
    while abs((E1./E2)-1)>0.000000000001    %accuracy of the result
        E2=E;
        E1=E+(M+e*sin(E)-E)./(1-e*cos(E));
        E=E1;
    end
    Result=E1;    %eccentric anomaly