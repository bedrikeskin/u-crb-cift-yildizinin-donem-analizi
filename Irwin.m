function [Result] = Irwin(pHJD,pA,pP3,pT3,pexcen,pomega)  %function to compute (O-C) from JDi, T0, A, p3, omega and excen

    Ex=kepler(pexcen,2*pi*(pHJD-pT3)/pP3);   %eccentric anomaly from Kepler equation
    ny=2*atan(sqrt((1+pexcen)/(1-pexcen))*tan(Ex/2));   %conversion of anomalies
    Oc=((1-pexcen^2)./(1+pexcen.*cos(ny)).*sin(ny+pomega*pi/180)+pexcen.*sin(pomega*pi/180))/sqrt(1-pexcen^2*((cos(pomega*pi/180))^2));    %basic equation to compute Light-Time effect
    
Result=pA*Oc;    %eccentric anomaly