function [fitresult, gof, object] = SinusFit(HJDminClfs, OeksiCpfs, agirlikSin, A, P3, T3, excen, omega)
[xData, yData, weights] = prepareCurveData( HJDminClfs, OeksiCpfs, agirlikSin );

ft = fittype( 'pA*((((1-pexcen^2)./(1+pexcen.*cos((2*atan(sqrt((1+pexcen)/(1-pexcen))*tan((kepler(pexcen,2*pi*(x-pT3)/pP3))/2))))).*sin((2*atan(sqrt((1+pexcen)/(1-pexcen))*tan((kepler(pexcen,2*pi*(x-pT3)/pP3))/2)))+pomega*pi/180)+pexcen.*sin(pomega*pi/180))/sqrt(1-pexcen^2*((cos(pomega*pi/180))^2))))', 'independent', 'x', 'dependent', 'y');
opts = fitoptions( ft );
opts.Display = 'Off';
% opts.Lower =      [0    20000  2400000     0          0];
opts.StartPoint = [A      P3     T3      excen    omega];
% opts.Upper =      [0.1  50000  2600000     1        360];
opts.Weights = weights;
% opts.Method = 'NonlinearLeastSquares';
% opts.TolX = 0.0000000;

[fitresult, gof, object] = fit( xData, yData, ft, opts );